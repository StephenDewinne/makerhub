import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BodyPipe } from './body.pipe';
import { HairPipe } from './hair.pipe';
import { BeardPipe } from './beard.pipe';
import { ShirtPipe } from './shirt.pipe';



@NgModule({
  declarations: [BodyPipe, HairPipe, BeardPipe, ShirtPipe],
  imports: [
    CommonModule
  ],
  exports:[BodyPipe,HairPipe, BeardPipe, ShirtPipe]
})
export class PipesModule { }
