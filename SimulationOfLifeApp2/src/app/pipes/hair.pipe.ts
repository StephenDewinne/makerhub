import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hair'
})
export class HairPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    return './../../assets/images/Character/Hair/' + value + '.png';
  }

}
