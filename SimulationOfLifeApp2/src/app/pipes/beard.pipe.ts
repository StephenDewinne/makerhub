import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'beard'
})
export class BeardPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string
  {
    return './../../assets/images/Character/Beard/' + value + '.png';
  }

}
