import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {Personnage, PersonnageComplet} from '../models/JoueurDetails';
import { Joueur } from '../models/Joueurs';

@Injectable({
  providedIn: 'root'
})
export class PersonnageService {

  constructor(private http: HttpClient) { }

  Post(perso : Personnage)
  {
    return this.http.post<Personnage>("http://localhost:55727/api/Personnage",
    {
      nom : perso.nom,
      sexe : perso.sexe,
      couleur : perso.couleur,
      couleurCheveux : perso.couleurCheveux,
      formeCheveux : perso.formeCheveux,
      formeBarbe : perso.formeBarbe,
      formeTop : perso.formeTop,
      formePant : perso.formePant,
      formeShoes : perso.formeShoes,
      couleurBarbe : perso.couleurBarbe,
      couleurTop : perso.couleurTop,
      couleurPant : perso.couleurPant,
      couleurShoes : perso.couleurShoes,
      pseudoJoueur : perso.pseudoJoueur})
  }

  Get()
  {
    return this.http.get<Personnage>("https://SimulationOfLife.somee.com/api/Personnage");
  }

  GetKhunAlive(pseudo : string)
  {
    return this.http.post<Personnage[]>("http://localhost:55727/api/Personnage/Khun", {pseudo});
  }

  UpdateKhun(idKhun : number)
  {
    return this.http.get<Personnage>("http://localhost:55727/api/Personnage/UpdateKhun/" + idKhun.toString());
  }

  SupprimerKhun(perso : PersonnageComplet)
    {
      perso.isAlive = false;
      perso.sante = 0;
      perso.sommeil = 0;
      perso.faim = 0;
      perso.hygiene = 0;
      perso.confort = 0;
      perso.divertissement = 0;
      return this.http.put<Personnage>("http://localhost:55727/api/Personnage", perso);
    }
}
