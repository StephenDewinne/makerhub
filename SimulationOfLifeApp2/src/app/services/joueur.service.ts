import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {JoueurDetails} from '../models/JoueurDetails';
import { Joueur } from '../models/Joueurs';
import { isNgTemplate } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class JoueurService {

  
  
  constructor(private http: HttpClient) { }

  getDetail()
  {
    let id = JSON.parse(localStorage.getItem("user")).id;
    return this.http.get<JoueurDetails>("https://SimulationOfLife.somee.com/api/Joueurs");
  }

  getAllJoueurs()
  {
    return this.http.get<Joueur>("https://SimulationOfLife.somee.com/api/Joueurs");
  }

  getJoueurByName(liste : string, name : string)
  {
    let a = JSON.parse(liste);
    for (let item in a)
    {
      if (a[item].pseudo == name)
      {
        return a[item];
      }
    }
  }
}
