import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Statistique } from '../models/Statistique';

@Injectable({
  providedIn: 'root'
})
export class StatistiqueService {

  constructor(private http: HttpClient) { }

  Get()
  {
    return this.http.get<Statistique>("https://SimulationOfLife.somee.com/api/Statistiques");
  }

  Post(id : number)
  {
    this.http.post<Statistique>("https://SimulationOfLife.somee.com/api/Statistiques",
    {
      idPersonnage : id,
      conduite : 0,
      cuisine : 0,
      jeuxVideos : 0,
      programmation : 0,
      ecriture : 0,
      bricolage : 0,
      jardinage : 0,
      fitness : 0,
      peche : 0,
      logique : 0,
      musique : 0
    }).subscribe(x => {});
  }
}
