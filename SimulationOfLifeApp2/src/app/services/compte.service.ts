import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CompteValide, Infos, Reponse } from '../models/Compte';

@Injectable({
  providedIn: 'root'
})
export class CompteService 
{  
  constructor(private http: HttpClient) { }

  getCheck(username : string, password : string)
  {
    return this.http.get("http://localhost:55727/api/Joueurs/" + username + "/" + password, {responseType : "text"})
  }

  createAccount(username : string, password : string)
  {
    return this.http.post<Reponse>("http://localhost:55727/api/Joueurs/" + username + "/" + password, {responseType : "json"});
  }

  getInfos(username : string)
  {
    return this.http.get<Infos>("http://localhost:55727/api/Joueurs/Infos/" + username);
  }

  updateAvatar(pseudo : string, avatar : string)
  {
    return this.http.put("http://localhost:55727/api/Joueurs", {pseudo, avatar});
  }
}
