export interface Statistique {
    id: number;
    idPersonnage : number;
    conduite : number;
    cuisine : number;
    jeuxVideos : number;
    programmation : number;
    ecriture : number;
    bricolage : number;
    jardinage : number;
    fitness : number;
    peche : number;
    logique : number;
    musique : number;
  }