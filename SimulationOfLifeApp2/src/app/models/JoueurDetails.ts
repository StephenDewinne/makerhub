export interface JoueurDetails {
  id: number;
  pseudo: string;
  motDePasse: string;
  pointsDeSurvie: number;
  personnages: Personnage[];
}

export interface Personnage {
  nom: string;
  sexe: string;
  couleur: number;
  formeCheveux : number;
  couleurCheveux: number;
  formeBarbe : number;
  couleurBarbe : number;
  formeTop : number;
  couleurTop : number;
  formePant : number;
  couleurPant : number;
  formeShoes : number;
  couleurShoes : number;
  pseudoJoueur : string;
}

export interface PersonnageComplet {
  id : number;
  idJoueur : number;
  nom: string;
  age : number;
  sexe: string;
  couleur: number;
  formeCheveux : number;
  couleurCheveux: number;
  formeBarbe : number;
  couleurBarbe : number;
  formeTop : number;
  couleurTop : number;
  formePant : number;
  couleurPant : number;
  formeShoes : number;
  couleurShoes : number;
  isAlive : boolean;
  sante : number;
  sommeil : number;
  faim : number;
  hygiene : number;
  confort : number;
  divertissement : number;
  argent : number;
}