export interface CompteValide
{
    compteValide : string;
}

export interface Reponse
{
    message : string;
}

export interface Infos
{
    pseudo : string;
    avatar : string;
    pointsDeSurvie : number;
}