import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Personnage, PersonnageComplet } from '../models/JoueurDetails';
import { Router } from '@angular/router';
import { PersonnageService } from '../services/personnage.service';
import { StatistiqueService } from '../services/statistique.service';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.page.html',
  styleUrls: ['./confirmation.page.scss'],
})
export class ConfirmationPage implements OnInit, AfterViewChecked
{

  public currentKhun : PersonnageComplet;
  public Jouer : boolean;
  public Supprimer : boolean;
  private stats;

  constructor(private router : Router, private persoService : PersonnageService, private statistiqueService : StatistiqueService)
  { 

  }

  Retour()
  {
    this.router.navigateByUrl('/main');
  }

  SupprimerKhun()
  {
    this.persoService.SupprimerKhun(this.currentKhun).toPromise().then(x =>
    {
      localStorage.setItem("EcranKhun", "true");
      localStorage.setItem("isReloaded", "false");
      this.router.navigateByUrl('/main');

      this.persoService.GetKhunAlive(localStorage.getItem("pseudo")).toPromise().then(x => 
      {
        localStorage.setItem("KhunAlive", JSON.stringify(x));
        window.location.reload();
      })
    })
  }

  JouerKhun()
  {
    this.statistiqueService.Get().subscribe(x =>
      {
        for (let item in x)
        {
  
          if (x[item].idPersonnage == this.currentKhun.id)
          {
            this.stats = x[item];
          }
        }
  
        if (this.stats == undefined)
        {
          this.statistiqueService.Post(this.currentKhun.id);
        }

        for (let item in x)
        {
  
          if (x[item].idPersonnage == this.currentKhun.id)
          {
            this.stats = x[item];
            localStorage.setItem("currentKhunStats", JSON.stringify(this.stats));
          }
        }
      });

    this.router.navigateByUrl('/intro');
  }

  ngOnInit()
  {
    this.currentKhun = JSON.parse(localStorage.getItem("currentKhun"));
    console.log(this.currentKhun);

    if (localStorage.getItem("confirmation") == "Jouer")
    {
      this.Jouer = true;
      this.Supprimer = false;
    }
    else
    {
      this.Jouer = false;
      this.Supprimer = true;
    }
  }

  ngAfterViewChecked()
  {

  }

}
