import { HttpClient } from '@angular/common/http';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Personnage } from '../models/JoueurDetails';
import { Statistique } from '../models/Statistique';
import { PersonnageService } from '../services/personnage.service';
import { StatistiqueService } from '../services/statistique.service';

@Component({
  selector: 'app-jeu',
  templateUrl: './jeu.page.html',
  styleUrls: ['./jeu.page.scss'],
})
export class JeuPage implements OnInit, AfterViewInit {

  public currentKhun : Personnage;

  public ListeChoixCuisine = [
    {
      id : 0,
      image : "./../../assets/images/PFI/Decors/Plat1.png",
      niveau : 0,
      titre : "Pâtes bolognèse",
      temps : "00:30",
      exp : 75,
      chanceBase : 80,
      chanceParNiveau : 5
    },
    {
      id : 1,
      image : "./../../assets/images/PFI/Decors/Plat2.png",
      niveau : 0,
      titre : "Omelette",
      temps : "00:45",
      exp : 100,
      chanceBase : 70,
      chanceParNiveau : 5
    },
    {
      id : 2,
      image : "./../../assets/images/PFI/Decors/Plat3.png",
      niveau : 1,
      titre : "Steak frites",
      temps : "01:00",
      exp : 125,
      chanceBase : 65,
      chanceParNiveau : 5
    },
    {
      id : 3, 
      image : "./../../assets/images/PFI/Decors/Plat4.png",
      niveau : 2,
      titre : "Poulet rôti",
      temps : "01:30",
      exp : 100,
      chanceBase : 70,
      chanceParNiveau : 5
    }
  ];

  public PlatChoisi = false;
  public NumeroPlatChoisi = 0;

  constructor(
    private statistiqueService : StatistiqueService,
    private http : HttpClient,
    public persoServices : PersonnageService
  )
  { 

  }

  // Liste des lieux disponibles
  public Lieux = [["./../../assets/images/PFI/Decors/Decor-Domicile.png", "Domicile"],["./../../assets/images/PFI/Decors/Decor-Magasin.png", "Magasin"],
                ["./../../assets/images/PFI/Decors/Decor-Lac.png", "Lac"]];

  // Variable utilisée pour faire défiler les lieux
  public compte;

  // Variable permettant l'affichage du menu du choix du lieu
  public choixDuLieu = false;

  // Lieu où se trouve le joueur indiqué par la valeur True
  public domicileCuisine = true;
  public domicileSalon = false;

  // Récupération du personnage depuis l'HTML
  private k;
  private imageKhun;

  // Récupération de la fen^étre affichée depuis l'HTML
  private win;

  // Sens dans lequel se dirige le personnage
  public directionKhun = "";

  // Texte du personnage quand il interagit avec des objets
  private _pensee = "";
  public penseeKhun;

  // Div affichant la pensée du personnage
  private bulleDePensee;

  // Pièce jusqu'où se déplace le personnage
  public _deplacement = "";
  public lieuDeDeplacement;

  // Div affichant le lieu de déplacement du personnage
  public bulleDeDeplacement;

  // Variable permettant au personnage de ne pas bouger lors d'un clic
  public dontMove = false;

  // Statistiques du personnage
  public stats : Statistique;

  // Interval de rafraîchissement des données
  public intervalId;

  // Liste des jauges de besoins
  public jauges;

  // Niveau de la compétence Cuisine
  public niveauCuisine = 0;

  // Action exécutée
  public _action = "";

  public bulleDAction;

  // Variable déterminant si le jeu est en pause
  public JeuEnPause = false;

  public c = 10;

  Defiler(sens : number)
  {
    if (sens == 1)
    {
      if (this.compte == this.Lieux.length - 1)
      {
        this.compte = 0;
      }

      else
      {
        this.compte += 1;
      }
    }

    else
    {
      if (this.compte == 0)
      {
        this.compte = this.Lieux.length - 1;
      }

      else
      {
        this.compte -= 1;
      }
    }
  }




  Aller()
  {
    console.log(this.Lieux[this.compte][1])

    if (this.compte == 0)
    {
      this.domicileSalon = true;
      this.domicileCuisine = false;
      this.choixDuLieu = false;
    }
  }




  MenuChoixDuLieu()
  {
    this.domicileSalon = false;
    this.domicileCuisine = false;
    this.choixDuLieu = true;
  }


  ChoisirPlat(id : number)
  {
    this.NumeroPlatChoisi = id;
    this.PlatChoisi = true;
  }



  RetourChoixDuPlat()
  {
    this.PlatChoisi = false;
  }



  getCoordinates(event)
  {
    this.imageKhun  = document.getElementById("imageKhun");
    this.k = document.getElementById("Khun");
    this.win = document.getElementById("window");
    this.bulleDePensee = document.getElementById("PenseeKhun");
    this.bulleDeDeplacement = document.getElementById("ConfirmationDeplacement");
    this.bulleDePensee.style.height = "0";
    this.bulleDePensee.style.top = "100%";
    this.bulleDeDeplacement.style.height = "0";
    this.bulleDeDeplacement.style.top = "100%";
    var value = this.k.style.left.split('px');

    var value2;

    if (value[1] == undefined)
    {
      value = this.k.style.left.split('%');
      value2 = (this.win.offsetWidth * value[0]) / 100;
    }
    else
    {
      value2 = parseInt(value[0]);
    }

    if (this.dontMove == false)
    {
      if(event.clientY > (this.win.offsetHeight / 10))
      {
        if (value2 < event.clientX - (this.imageKhun.offsetWidth / 2))
        {
          this.directionKhun = "_Droite";
        }
        else
        {
          this.directionKhun = "_Gauche";
        }
        this.k.style.left = (event.clientX - (this.imageKhun.offsetWidth / 2)).toString() + "px";
      }
    }
    
    if(this._action == "")
    {
      this.dontMove = false;
    }
    

    this.k.ontransitionend = () =>
    {
      this.bulleDePensee = document.getElementById("PenseeKhun");
      this.bulleDeDeplacement = document.getElementById("ConfirmationDeplacement");
      
      this.directionKhun = "";

      if (this._pensee != "")
      {
        this.bulleDePensee.style.height = "35%";
        this.bulleDePensee.style.top = "60%";
        this.penseeKhun = this._pensee;
        this._pensee = "";
      }

      else if (this._deplacement != "")
      {
        this.bulleDeDeplacement.style.height = "35%";
        this.bulleDeDeplacement.style.top = "60%";
        this.lieuDeDeplacement = this._deplacement;
        this._deplacement = "";
      }

      else
      {
        this.penseeKhun = "";
        this.lieuDeDeplacement = "";
        this.bulleDePensee.style.height = "0";
        this.bulleDePensee.style.top = "100%";
        this.bulleDeDeplacement.style.height = "0";
        this.bulleDeDeplacement.style.top = "100%";
      }

      if (this._action != "")
      {
        this.bulleDAction.style.top = "0";
        this.dontMove = true;
      }

      else
      {
        // this.bulleDAction.style.top = "100%";
        this.dontMove = false;
      }
      
    };
  }




  DefilerBesoins()
  {
    document.getElementById("Besoins").style.height = '50%';
  }




  RemonterBesoins()
  {
    document.getElementById("Besoins").style.height = '0';
  }




  Penser(objet : string)
  {
    if (objet == "cadre")
    {
      this._pensee = "Je n'ai aucune idée de qui sont ces personnes sur la photo..."
    }
    else if (objet == "television")
    {
      this._pensee = "Aaaaah la télé... Rien de tel que Netflix !"
    }
    else if (objet == "evier")
    {
      this._pensee = "Quelle étrange sensation ... Un évier vide et propre !"
    }
    else if (objet == "cuisine")
    {
      this._pensee = "Cette magnifique cuisine provient d'un artiste Suédois de renom !"
    }
    else
    {
      console.log("erreur")
    }
  }



  AllerVers(lieu : string)
  {
    this._deplacement = lieu;
  }




  Conf()
  {

    if (this.lieuDeDeplacement == "la cuisine")
    {
      this.domicileSalon = false;
      this.domicileCuisine = true;
    }

    else if (this.lieuDeDeplacement == "le salon")
    {
      this.domicileSalon = true;
      this.domicileCuisine = false;
    }
  }



  DontMove()
  {
    this.dontMove = true;
  }

  Action(action : string)
  {
    this.bulleDAction = document.getElementById(action);
    this._action = action;
    clearInterval(this.intervalId);
    // // A CHANGER
    // let niveau = 200;
    // let temp = 0;
    // this.stats.cuisine += 50;
    // while(niveau <= this.stats.cuisine)
    // {
    //   temp = niveau;
    //   niveau += niveau * 1.5;
    //   this.niveauCuisine += 1;
    // }
    // document.getElementById("barre_cuisine").style.height = (((this.stats.cuisine - temp) / (niveau - temp)) * 100).toString() + "%";
  }

  QuitterAction()
  {
    this._action = "";
    this.bulleDAction.style.top = "100%";
    this.PlatChoisi = false;
    this.intervalId = setInterval(this.RafraichissementDesDonnees, 1500, this.currentKhun);
  }

  RafraichissementDesDonnees(khun : any)
  {
    this.jauges = document.getElementsByClassName("jauges_barre_barre");

    console.log(this.stats.idPersonnage)

    if(khun.faim <= 0)
    {
      khun.faim = 0;
      khun.sante -= 4;
    }
    else if (khun.faim < 30)
    {
      this.jauges[1].style.backgroundColor = "red";
    }
    else if (khun.faim < 50)
    {
      this.jauges[1].style.backgroundColor = "orange";
    }
    else if (khun.faim < 70)
    {
      this.jauges[1].style.backgroundColor = "yellow";
    }
    else if (khun.faim < 85)
    {
      this.jauges[1].style.backgroundColor = "yellowgreen";
    }
    else
    {
      this.jauges[1].style.backgroundColor = "green";
    }

    if (khun.hygiene < 30)
    {
      this.jauges[2].style.backgroundColor = "red";
    }
    else if (khun.hygiene < 50)
    {
      this.jauges[2].style.backgroundColor = "orange";
    }
    else if (khun.hygiene < 70)
    {
      this.jauges[2].style.backgroundColor = "yellow";
    }
    else if (khun.hygiene < 85)
    {
      this.jauges[2].style.backgroundColor = "yellowgreen";
    }
    else
    {
      this.jauges[2].style.backgroundColor = "green";
    }


    if(khun.sante <= 0)
    {
      // RIP gamin
      khun.sante = 0;
    }
    else if (khun.sante < 30)
    {
      this.jauges[0].style.backgroundColor = "red";
    }
    else if (khun.sante < 50)
    {
      this.jauges[0].style.backgroundColor = "orange";
    }
    else if (khun.sante < 70)
    {
      this.jauges[0].style.backgroundColor = "yellow";
    }
    else if (khun.sante < 85)
    {
      this.jauges[0].style.backgroundColor = "yellowgreen";
    }
    else
    {
      this.jauges[0].style.backgroundColor = "green";
    }

    this.jauges[0].style.height = (khun.sante).toString() + "%";
    this.jauges[1].style.height = (khun.faim).toString() + "%";
    this.jauges[2].style.height = (khun.hygiene).toString() + "%";
    
  }



  ngOnInit()
  {
    var cont = 0;
    this.compte = 0;
    this.currentKhun = JSON.parse(localStorage.getItem("currentKhun"));
    console.log(this.currentKhun);
    this.stats = JSON.parse(localStorage.getItem("currentKhunStats"))
    this.intervalId = setInterval(() => 
    {
      this.persoServices.UpdateKhun(this.stats.idPersonnage).toPromise().then(x => {
        console.log(x);
        this.currentKhun = x;
        this.RafraichissementDesDonnees(x);
      })
    }, 3000);
  }

  ngOnDestroy()
  {
    clearInterval(this.intervalId);
  }




  ngAfterViewInit()
  {
    this.k = document.getElementById("Khun");
  }

}
