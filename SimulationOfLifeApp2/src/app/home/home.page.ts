import { Component, OnInit, AfterContentInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Joueur } from '../models/Joueurs';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Move } from './FondAnime';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { JoueurService } from '../services/joueur.service';
import { CompteService } from '../services/compte.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements AfterContentInit {

  public username;
  public password;
  public message;
  public rem;
  private existingUser = false;

  constructor(
    private http: HttpClient,
    public toastController: ToastController,
    private router : Router,
    private joueurService : JoueurService,
    private compteService : CompteService
  )
  {
    
  }

  public joueur : Joueur
  public listeDeJoueurs : Joueur[]
  public listeDesJoueurs : string[]
  public j : string

  
  public texte1 : string;
  public texte2 : string;
  public texte3 : string;
  public texte4 : string;
  public texte5 : string;
  public texte6 : string;
  public texte7 : string;
  public texte8 : string;

  public setupFrancais()
  {
    this.closeNav();
    localStorage.setItem("Langue", "Français");
    this.texte1 = "Nom d'utilisateur";
    this.texte2 = "Mot de passe";
    this.texte3 = "Se connecter";
    this.texte4 = "Créer un compte";
    this.texte5 = "Connexion réussie !";
    this.texte6 = "Mot de passe incorrect !";
    this.texte7 = "Ce nom d'utilisateur n'existe pas !";
    this.texte8 = "Veuillez remplir tous les champs !";
  }

  public setupAnglais()
  {
    this.closeNav();
    localStorage.setItem("Langue", "Anglais");
    this.texte1 = "Username";
    this.texte2 = "Password";
    this.texte3 = "Log in";
    this.texte4 = "Create account";
    this.texte5 = "Successful connection !";
    this.texte6 = "Wrong password !";
    this.texte7 = "This username doesn't exist !";
    this.texte8 = "Please, complete all fields !";
  }

  /* Set the width of the side navigation to 250px */
  public openNav()
  {
  document.getElementById("mySidenav").style.width = "250px";
  }

/* Set the width of the side navigation to 0 */
  public closeNav()
  {
  document.getElementById("mySidenav").style.width = "0";
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: this.texte5,
      duration: 2000
    });
    toast.present();
  }

  public fillInput()
  {
    let re = (<HTMLInputElement>document.getElementById("seSouvenir"));
    this.username = localStorage.getItem("pseudo");
    this.password = localStorage.getItem("mdp");
    re.checked = true;
  }

  public clearInput()
  {
    let re = (<HTMLInputElement>document.getElementById("seSouvenir"));
    this.username = "";
    this.password = "";
    re.checked = false;
  }

  public Login():void
  {
    let remember = (<HTMLInputElement>document.getElementById("seSouvenir"));
    let stayTuned = (<HTMLInputElement>document.getElementById("resterCo"));
    this.message = "";
    let b;

    if ((this.username != undefined && this.username != "") && (this.password != undefined && this.password != ""))
    {
      this.compteService.getCheck(this.username, this.password).subscribe(x =>
      {
        if (x == "Valide")
        {
          if (remember.checked == true)
          {
            localStorage.setItem("pseudo", this.username)
            localStorage.setItem("mdp", this.password)
            localStorage.setItem("rem", "true")
          }

          else
          {
            this.username = "";
            this.password = "";
            localStorage.setItem("pseudo", "")
            localStorage.setItem("mdp", "")
            localStorage.setItem("rem", "false")
          }

          if (stayTuned.checked == true)
          {
            localStorage.setItem("resterCo", "true")
          }

          else
          {
            localStorage.setItem("resterCo", "false")
          }

          this.router.navigateByUrl("/main");
        }

        else if (x == "BadPassword")
        {
          this.message = this.texte6;
        }

        else if (x == "BadPseudo")
        {
          this.message = this.texte7;
        }
      })
    }

    else
    {
      this.message = this.texte8;
    }
  }

  ngAfterContentInit()
  {

    localStorage.setItem("menu", "menu");

    if (localStorage.getItem("resterCo") == "true")
    {
      this.router.navigateByUrl("/main");
    }

    if (localStorage.getItem("Langue") == "Anglais")
    {
      this.setupAnglais()
    }

    else
    {
      this.setupFrancais()
    }

    this.rem = localStorage.getItem("rem");

    if (this.rem == "true")
    {
      this.fillInput();
    }

    else
    {
      this.clearInput();
    }
  }
}
