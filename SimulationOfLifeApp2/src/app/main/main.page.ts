import { Component, OnInit, AfterContentInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { JoueurService } from '../services/joueur.service';
import { PersonnageService } from '../services/personnage.service';
import { HttpClient } from '@angular/common/http';
import { Personnage } from '../models/JoueurDetails';
import { Router } from '@angular/router';
import { CompteService } from '../services/compte.service';
import { Infos } from '../models/Compte';
import { Avatar } from '../models/Avatar';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {

  constructor(
    private joueurService : JoueurService,
    private persoService : PersonnageService,
    private http : HttpClient,
    private router : Router,
    private compteService : CompteService) { }

  public infosJoueur : Infos;
  public pseudo : string;
  public listePerso : Personnage[];
  public show1 : boolean = false;
  public show2 : boolean = false;
  public show3 : boolean = false;
  public show4 : boolean = false;
  public show5 : boolean = false;
  public test : number = 101;

  public nombreCartesDeVisite = [0, 1, 2, 3, 4];

  public avatar : string = "Avatar_Stephen";
  public avatarChoisi : string;
  public descriptif : string;
  public prenomAvatar : string;
  public points : number = 0;
  public listeAvatar = 
  [
    ["Avatar_Stephen", "''Boire du Monster empêche de dormir. Par contre, dormir empêche de boire du Monster !''"],
    ["Avatar_Lum", "''Ce soir, je comptes bien m'éclater !''"],
    ["Avatar_Adrien", "''Dans le nord on dit un Orval, dans le sud une Orval, chez moi on dit deux !''"],
    ["Avatar_Alexandr", "''сука блядь !''"],
    ["Avatar_Mathieu", "''GO 1V1 CS NOOBZ !''"],
    ["Avatar_Thomas", "''Et on fait tourner les ... serviettes !''"],
    ["Avatar_Florian", "''J'ai bientôt ma caisse pour le concours de drifts, Sereno !''"],
    ["Avatar_Maxime", "''Ça à l'air assez glissant pour ... DÉJA VUUU !''"],
    ["Avatar_Meghane", "''Je vois pas de quoi vous parlez, moi j'ai kiffé React.js !''"],
    ["Avatar_Sereno", "''Un vaccin vaut mieux que deux 'tu l'auras' !''"],
    ["Avatar_Khun", "''Je m'appelle Eikichi Onizuka, 22 ans, célibataire et libre comme l'air !'"],
    ["Avatar_Logan", "''Je vous prends tous sur FIFA ! Sauf Lum, ce bg''"]
  ];

  public Instructions()
  {
    let selectionAvatar = (<HTMLDivElement>document.getElementById('selectionAvatar'));
    selectionAvatar.style.display = "none";
    let selectionPerso = (<HTMLDivElement>document.getElementById('selectionPerso'));
    selectionPerso.style.display = "none";
    let menu = (<HTMLDivElement>document.getElementById('menu'));
    menu.style.display = "none";
    let instructions = (<HTMLDivElement>document.getElementById('instructions'));
    instructions.style.display = "flex";
  }

  public InstructionToMenu()
  {
    let selectionAvatar = (<HTMLDivElement>document.getElementById('selectionAvatar'));
    selectionAvatar.style.display = "none";
    let selectionPerso = (<HTMLDivElement>document.getElementById('selectionPerso'));
    selectionPerso.style.display = "none";
    let menu = (<HTMLDivElement>document.getElementById('menu'));
    menu.style.display = "flex";
    let instructions = (<HTMLDivElement>document.getElementById('instructions'));
    instructions.style.display = "none";
  }

  public SelectionPerso()
  {
    let selectionAvatar = (<HTMLDivElement>document.getElementById('selectionAvatar'));
    selectionAvatar.style.display = "none";
    let selectionPerso = (<HTMLDivElement>document.getElementById('selectionPerso'));
    selectionPerso.style.display = "flex";
    let menu = (<HTMLDivElement>document.getElementById('menu'));
    menu.style.display = "none";
    let instructions = (<HTMLDivElement>document.getElementById('instructions'));
    instructions.style.display = "none";

    this.persoService.GetKhunAlive(this.pseudo).subscribe(x =>
    {
      if (this.listePerso == undefined)
      {
        this.listePerso = x;
        localStorage.setItem("KhunAlive", JSON.stringify(x));
      }
      else
      {
        this.listePerso = JSON.parse(localStorage.getItem("KhunAlive"));
      }
      for (let item of this.nombreCartesDeVisite)
      {
        if (this.listePerso[item] == undefined)
        {
          this.listePerso.push(null);
        }
      }
    })
  }

  public SelectionToMenu()
  {
    let selectionAvatar = (<HTMLDivElement>document.getElementById('selectionAvatar'));
    selectionAvatar.style.display = "none";
    let selectionPerso = (<HTMLDivElement>document.getElementById('selectionPerso'));
    selectionPerso.style.display = "none";
    let menu = (<HTMLDivElement>document.getElementById('menu'));
    menu.style.display = "flex";
    let instructions = (<HTMLDivElement>document.getElementById('instructions'));
    instructions.style.display = "none";
  }

  public SupprimerKhun(khun : any)
  {
    localStorage.setItem('currentKhun', JSON.stringify(khun));
    localStorage.setItem('confirmation', "Supprimer");
    this.router.navigateByUrl('/confirmation');
  }

  public JouerKhun(khun : any)
  {
    localStorage.setItem('currentKhun', JSON.stringify(khun));
    localStorage.setItem('confirmation', "Jouer");
    this.router.navigateByUrl('/confirmation');
  }

  public seDeco()
  {
    localStorage.setItem("resterCo", "false");
    this.router.navigateByUrl("/home");
  }

  public SelectionAvatar()
  {
    let selectionAvatar = (<HTMLDivElement>document.getElementById('selectionAvatar'));
    selectionAvatar.style.display = "flex";
    let selectionPerso = (<HTMLDivElement>document.getElementById('selectionPerso'));
    selectionPerso.style.display = "none";
    let menu = (<HTMLDivElement>document.getElementById('menu'));
    menu.style.display = "none";
    let instructions = (<HTMLDivElement>document.getElementById('instructions'));
    instructions.style.display = "none";
    let boutonDefinir = (<HTMLButtonElement>document.getElementById('bouton_definir'));
    boutonDefinir.style.display = "none";
  }

  public SelectionAvatarToMenu()
  {
    let selectionAvatar = (<HTMLDivElement>document.getElementById('selectionAvatar'));
    selectionAvatar.style.display = "none";
    let selectionPerso = (<HTMLDivElement>document.getElementById('selectionPerso'));
    selectionPerso.style.display = "none";
    let menu = (<HTMLDivElement>document.getElementById('menu'));
    menu.style.display = "flex";
    let instructions = (<HTMLDivElement>document.getElementById('instructions'));
    instructions.style.display = "none";
  }

  public setAvatar()
  {
    this.compteService.updateAvatar(this.pseudo, this.avatarChoisi).subscribe(x =>
    {
      this.avatar = this.avatarChoisi;
      console.log(x);
      this.compteService.getInfos(this.pseudo)
      .subscribe(x =>
      {
        this.avatar = x[0].avatar;

        this.affichageBoutonDefinir();
      })
    })
  }

  public affichageBoutonDefinir()
  {
    let boutonDefinir = (<HTMLButtonElement>document.getElementById('bouton_definir'));

    if (this.avatarChoisi == this.avatar)
    {
      boutonDefinir.style.display = "none";
    }

    else
    {
      boutonDefinir.style.display = "flex";
    }
  }

  public clickAvatar(item : string)
  {
    this.avatarChoisi = item;

    for (let item of this.listeAvatar)
    {
      if (item[0] == this.avatarChoisi)
      {
        this.descriptif = item[1];
        this.prenom(item[0]);
      }
    }

    this.affichageBoutonDefinir();
  }

  public prenom(item : string)
  {
    this.prenomAvatar = item.split("_", 2)[1];
  }

  update()
  {
    console.log(this.infosJoueur);
  }

  ngOnInit()
  {
    
    if (localStorage.getItem("EcranKhun") == "true")
    {
      if (localStorage.getItem("isReloaded") == "false")
      {
        localStorage.setItem("isReloaded", "true");
        window.location.reload();
      }
      
      else
      {
        localStorage.setItem("EcranKhun", "false");
        this.listePerso = JSON.parse(localStorage.getItem("KhunAlive"));
        this.SelectionPerso();
      }
    }

    this.descriptif = this.listeAvatar[0][1];

    this.pseudo = localStorage.getItem("pseudo");

    let promesse = new Promise((resolve, reject) =>
    {
      this.compteService.getInfos(this.pseudo)
      .subscribe(x =>
      {
        this.infosJoueur = x;
        this.pseudo = x[0].pseudo;
        this.avatar = x[0].avatar;
        this.avatarChoisi = this.avatar;

        for (let item of this.listeAvatar)
        {
          if (item[0] == this.avatarChoisi)
          {
            this.descriptif = item[1];
          }
        }

        this.prenom(this.avatarChoisi);
        this.points = x[0].pointsDeSurvie;
        resolve(true);
      })
    })

  }
}
