import { Component, OnInit, SkipSelf } from '@angular/core';
import { fonction1 } from './anim';
import {Router} from '@angular/router';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {
  
  constructor(private router: Router)
  {
  }

  public skip()
  {
    this.router.navigateByUrl("/jeu");
  }

  ngOnInit()
  {
    let doc1 = document.getElementById("intro1") as HTMLImageElement;
    let intro : string;
    let intro1 : string = "./../../assets/images/Intro1.png";
    let intro2 : string = "./../../assets/images/Intro2.png";
    let intro3 : string = "./../../assets/images/Logo4.png";

    let interval;

    let jsp = false;
    let jsp2 = window.addEventListener("keydown", function(e)
    {
      jsp = true;
      return true;
    });
    
    doc1.style.opacity = "0";
    doc1.src = intro1;
    
    fonction1(doc1, intro1, intro2, intro3);

    interval = setInterval(() =>
    {
      if (jsp == true)
      {
        clearInterval(interval);
        this.skip();
      }
    }, 10);
  }

}
