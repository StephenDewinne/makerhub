import { Component, OnInit } from '@angular/core';
import { JoueurService } from '../services/joueur.service';
import { PersonnageService } from '../services/personnage.service';
import { HttpClient } from '@angular/common/http';
import { Personnage } from '../models/JoueurDetails';
import { Router } from '@angular/router';

@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.css']
})
export class CreationComponent implements OnInit {

  public nomKhun;
  public genreKhun;
  public genreKhunBase = "M";
  public couleurCorpsKhun = 1;
  public couleurKhun;
  public couleurCheveuxKhun = 1;
  public formeCheveuxKhun = 1;
  public couleurBarbeKhun = 1;
  public formeBarbeKhun = 1;
  public pilositeKhun;
  public isMasculin = true;
  public formeTopKhun = 1;
  public formePantsKhun = 1;
  public formeShoesKhun = 1;
  public couleurTopKhun = 1;
  public couleurPantsKhun = 1;
  public couleurShoesKhun = 1;
  public infosJoueur;
  public perso : Personnage;

  constructor(
    private http: HttpClient,
    private joueurService : JoueurService,
    private persoService : PersonnageService,
    private router : Router
    )
    { }

  Nom()
  {
    document.getElementById("div_creation").animate(
      [
        {left:'0px'}
      ],
      {
        duration:500,
        fill:"forwards"
      });
  }

  Genre()
  {
    document.getElementById("div_creation").animate(
      [
        {left:'-70%'}
      ],
      {
        duration:500,
        fill:"forwards"
      });
  }

  Corps()
  {
    document.getElementById("div_creation").animate(
      [
        {left:'-140%'}
      ],
      {
        duration:500,
        fill:"forwards"
      });
  }

  Cheveux()
  {
    document.getElementById("div_creation").animate(
      [
        {left:'-210%'}
      ],
      {
        duration:500,
        fill:"forwards"
      });
  }

  Habits()
  {
    document.getElementById("div_creation").animate(
      [
        {left:'-280%'}
      ],
      {
        duration:500,
        fill:"forwards"
      });
  }

  ValiderPrenom()
  {
    if (this.nomKhun.length > 0)
    {
      document.getElementById("div_creation").animate(
        [
          {left:'-70%'}
        ],
        {
          duration:500,
          fill:"forwards"
        });
    }
  }

  ValiderGenre(choix : string)
  {
    this.genreKhun = choix;
    this.genreKhunBase = choix;

    if (choix == "M")
    {
      this.isMasculin = true;
    }

    else
    {
      this.isMasculin = false;
      this.formeBarbeKhun = 1;
    }

    document.getElementById("div_creation").animate(
      [
        {left:'-140%'}
      ],
      {
        duration:500,
        fill:"forwards"
      });
  }

  Defiler(sens : number)
  {
    if (sens == -1)
    {
      if (this.couleurCorpsKhun == 1)
      {
        this.couleurCorpsKhun = 8;
      }
      else
      {
        this.couleurCorpsKhun += sens;
      }
    }
    else
    {
      if (this.couleurCorpsKhun == 8)
      {
        this.couleurCorpsKhun = 1;
      }
      else
      {
        this.couleurCorpsKhun += sens;
      }
    }
  }

  ValiderCorps()
  {
    if (this.couleurCorpsKhun == 1 || this.couleurCorpsKhun == 2 || this.couleurCorpsKhun == 3)
    {
      this.couleurKhun = "Blanc"
    }
    else if (this.couleurCorpsKhun == 4 || this.couleurCorpsKhun == 5 || this.couleurCorpsKhun == 6)
    {
      this.couleurKhun = "Métisse"
    }
    else if (this.couleurCorpsKhun == 7 || this.couleurCorpsKhun == 8)
    {
      this.couleurKhun = "Noir"
    }

    document.getElementById("div_creation").animate(
      [
        {left:'-210%'}
      ],
      {
        duration:500,
        fill:"forwards"
      });
  }

  DefilerCheveux(sens : number)
  {
    if (sens == -1)
    {
      if (this.formeCheveuxKhun == 1)
      {
        this.formeCheveuxKhun = 10;
      }
      else
      {
        this.formeCheveuxKhun += sens;
      }
    }
    else
    {
      if (this.formeCheveuxKhun == 10)
      {
        this.formeCheveuxKhun = 1;
      }
      else
      {
        this.formeCheveuxKhun += sens;
      }
    }
  }

  DefilerCouleurCheveux(sens : number)
  {
    if (sens == -1)
    {
      if (this.couleurCheveuxKhun == 1)
      {
        this.couleurCheveuxKhun = 6;
      }
      else
      {
        this.couleurCheveuxKhun += sens;
      }
    }
    else
    {
      if (this.couleurCheveuxKhun == 6)
      {
        this.couleurCheveuxKhun = 1;
      }
      else
      {
        this.couleurCheveuxKhun += sens;
      }
    }
  }

  DefilerBarbe(sens : number)
  {
    if (sens == -1)
    {
      if (this.formeBarbeKhun == 1)
      {
        this.formeBarbeKhun = 5;
      }
      else
      {
        this.formeBarbeKhun += sens;
      }
    }
    else
    {
      if (this.formeBarbeKhun == 5)
      {
        this.formeBarbeKhun = 1;
      }
      else
      {
        this.formeBarbeKhun += sens;
      }
    }
  }

  DefilerCouleurBarbe(sens : number)
  {
    if (sens == -1)
    {
      if (this.couleurBarbeKhun == 1)
      {
        this.couleurBarbeKhun = 6;
      }
      else
      {
        this.couleurBarbeKhun += sens;
      }
    }
    else
    {
      if (this.couleurBarbeKhun == 6)
      {
        this.couleurBarbeKhun = 1;
      }
      else
      {
        this.couleurBarbeKhun += sens;
      }
    }
  }

  ValiderCheveux()
  {
    if (this.formeCheveuxKhun == 1)
    {
      this.pilositeKhun = "Chauve";
    }
    else
    {
      if (this.couleurCheveuxKhun == 1)
      {
        this.pilositeKhun = "Noir";
      }
      else if (this.couleurCheveuxKhun == 2)
      {
        this.pilositeKhun = "Blond";
      }
      else if (this.couleurCheveuxKhun == 3)
      {
        this.pilositeKhun = "Brun";
      }
      else if (this.couleurCheveuxKhun == 4)
      {
        this.pilositeKhun = "Gris";
      }
      else if (this.couleurCheveuxKhun == 5)
      {
        this.pilositeKhun = "Blanc";
      }
      else if (this.couleurCheveuxKhun == 6)
      {
        this.pilositeKhun = "Roux";
      }
    }

    document.getElementById("div_creation").animate(
      [
        {left:'-280%'}
      ],
      {
        duration:500,
        fill:"forwards"
      });
  }

  DefilerTop(sens : number)
  {
    if (sens == -1)
    {
      if (this.formeTopKhun == 1)
      {
        this.formeTopKhun = 2;
      }
      else
      {
        this.formeTopKhun += sens;
      }
    }
    else
    {
      if (this.formeTopKhun == 2)
      {
        this.formeTopKhun = 1;
      }
      else
      {
        this.formeTopKhun += sens;
      }
    }
  }

  DefilerCouleurTop(sens : number)
  {
    if (sens == -1)
    {
      if (this.couleurTopKhun == 1)
      {
        this.couleurTopKhun = 12;
      }
      else
      {
        this.couleurTopKhun += sens;
      }
    }
    else
    {
      if (this.couleurTopKhun == 12)
      {
        this.couleurTopKhun = 1;
      }
      else
      {
        this.couleurTopKhun += sens;
      }
    }
  }

  DefilerPant(sens : number)
  {
    if (sens == -1)
    {
      if (this.formePantsKhun == 1)
      {
        this.formePantsKhun = 3;
      }
      else
      {
        this.formePantsKhun += sens;
      }
    }
    else
    {
      if (this.formePantsKhun == 3)
      {
        this.formePantsKhun = 1;
      }
      else
      {
        this.formePantsKhun += sens;
      }
    }
  }

  DefilerCouleurPant(sens : number)
  {
    if (sens == -1)
    {
      if (this.couleurPantsKhun == 1)
      {
        this.couleurPantsKhun = 6;
      }
      else
      {
        this.couleurPantsKhun += sens;
      }
    }
    else
    {
      if (this.couleurPantsKhun == 6)
      {
        this.couleurPantsKhun = 1;
      }
      else
      {
        this.couleurPantsKhun += sens;
      }
    }
  }

  DefilerShoes(sens : number)
  {
    if (sens == -1)
    {
      if (this.formeShoesKhun == 1)
      {
        this.formeShoesKhun = 2;
      }
      else
      {
        this.formeShoesKhun += sens;
      }
    }
    else
    {
      if (this.formeShoesKhun == 2)
      {
        this.formeShoesKhun = 1;
      }
      else
      {
        this.formeShoesKhun += sens;
      }
    }
  }

  DefilerCouleurShoes(sens : number)
  {
    if (sens == -1)
    {
      if (this.couleurShoesKhun == 1)
      {
        this.couleurShoesKhun = 5;
      }
      else
      {
        this.couleurShoesKhun += sens;
      }
    }
    else
    {
      if (this.couleurShoesKhun == 5)
      {
        this.couleurShoesKhun = 1;
      }
      else
      {
        this.couleurShoesKhun += sens;
      }
    }
  }

  ValiderKhun()
  {
    document.getElementById("div_creation").animate(
      [
        {left:'-350%'}
      ],
      {
        duration:500,
        fill:"forwards"
      });
  }

  AjouterKhun()
  {
    var perso : Personnage =
    {
      nom : this.nomKhun,
      couleur : this.couleurCorpsKhun,
      sexe : this.genreKhun,
      couleurBarbe : this.couleurBarbeKhun,
      couleurCheveux : this.couleurCheveuxKhun,
      couleurPant : this.couleurPantsKhun,
      couleurShoes : this.couleurShoesKhun,
      couleurTop : this.couleurTopKhun,
      formeBarbe : this.formeBarbeKhun,
      formeCheveux : this.formeCheveuxKhun,
      formePant : this.formePantsKhun,
      formeShoes : this.formeShoesKhun,
      formeTop : this.formeTopKhun,
      pseudoJoueur : this.infosJoueur
    }
    
    this.persoService.Post(perso).subscribe(x => 
    {
      localStorage.setItem("EcranKhun", "true");
      localStorage.setItem("isReloaded", "false");
      this.router.navigateByUrl('/main');
      
      this.persoService.GetKhunAlive(localStorage.getItem("pseudo")).toPromise().then(x =>
      {
        localStorage.setItem("KhunAlive", JSON.stringify(x));
        window.location.reload();
      })
    })
    
  }

  ngOnInit(): void
  {
    this.infosJoueur = localStorage.getItem("pseudo");
  }

}
