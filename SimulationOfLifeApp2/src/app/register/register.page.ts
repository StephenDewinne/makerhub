import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HomePage } from '../home/home.page';
import { CompteService } from '../services/compte.service';
import { Reponse } from '../models/Compte';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(
    private http: HttpClient,
    public toastController: ToastController,
    private router : Router,
    private compteService : CompteService
  ) { }

  public username : string;
  public password1 : string;
  public password2 : string;
  public message : string;
  public compte : number;

  public texte1 : string;
  public texte2 : string;
  public texte3 : string;
  public texte4 : string;
  public texte5 : string;
  public texte6 : string;
  public texte7 : string;
  public texte8 : string;
  public texte9 : string;
  public texte10 : string;

  public setupFrancais()
  {
    localStorage.setItem("Langue", "Français");
    this.texte1 = "Compte créé avec succès !";
    this.texte2 = "Vos mots de passe sont différents !";
    this.texte3 = "Ce pseudo est déjà prit !";
    this.texte4 = "Votre nom doit faire au moins 6 charactères !";
    this.texte5 = "Veuillez renseigner tous les champs !";
    this.texte6 = "Nom d'utilisateur *";
    this.texte7 = "Mot de passe *";
    this.texte8 = "Confirmez le mot de passe *";
    this.texte9 = "(*) Champ requis.";
    this.texte10 = "Créer un compte";
  }

  public setupAnglais()
  {
    localStorage.setItem("Langue", "Anglais");
    this.texte1 = "Account successfully created !";
    this.texte2 = "Your passwords are different !";
    this.texte3 = "This username is already taken !";
    this.texte4 = "Your name must be at least 6 characters long !";
    this.texte5 = "Please, complete all fields !";
    this.texte6 = "Username *";
    this.texte7 = "Password *";
    this.texte8 = "Confirm password *";
    this.texte9 = "(*) Required field.";
    this.texte10 = "Create account";
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: this.texte1,
      duration: 2000
    });
    toast.present();
  }

  public valider():void
  {
    var reponse : Reponse;
    let newUser = true;
    this.message = '';
    if (this.username != undefined && this.password1 != undefined && this.password2 != undefined)
    {
      if (this.username.length >= 6)
      {
          if (this.password1 == this.password2)
          {
            this.compteService.createAccount(this.username, this.password1).subscribe(x => {
              
              if (x.message == "Le pseudo existe déjà")
              {
                this.message = this.texte3;
              }
              else if (x.message == "Compte crée")
              {
                this.presentToast();
                this.router.navigateByUrl("/home");
              }
            });
          }

          else
          {
            this.message = this.texte2;
          }
      }

      else
      {
        this.message = this.texte4;
      }
    }

    else
    {
      this.message = this.texte5;
    }

    
  }

  ngOnInit() 
  {
    if (localStorage.getItem("Langue") == "Anglais")
    {
      this.setupAnglais()
    }

    else
    {
      this.setupFrancais()
    }

    
  }

}
