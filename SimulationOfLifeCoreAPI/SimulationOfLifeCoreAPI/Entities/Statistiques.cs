﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.Entities
{
    public class Statistiques
    {
        public int Id { get; set; }
        public int IdPersonnage { get; set; }
        public int Conduite { get; set; }
        public int Cuisine { get; set; }
        public int JeuxVideos { get; set; }
        public int Programmation { get; set; }
        public int Ecriture { get; set; }
        public int Bricolage { get; set; }
        public int Jardinage { get; set; }
        public int Fitness { get; set; }
        public int Peche { get; set; }
        public int Logique { get; set; }
        public int Musique { get; set; }
    }
}
