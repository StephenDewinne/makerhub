﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.Entities
{
    public class Infos
    {
        public string Pseudo { get; set; }
        public string Avatar { get; set; }
        public int PointsDeSurvie { get; set; }
    }
}
