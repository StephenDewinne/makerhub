﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.Entities
{
    public class SuccesJoueur
    {
        public int Id { get; set; }
        public int IdJoueur { get; set; }
        public int IdSucces { get; set; }
    }
}
