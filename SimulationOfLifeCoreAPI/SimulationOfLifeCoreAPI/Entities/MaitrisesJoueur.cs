﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.Entities
{
    public class MaitrisesJoueur
    {
        public int Id { get; set; }
        public int IdJoueur { get; set; }
        public int IdMaitrise { get; set; }
    }
}
