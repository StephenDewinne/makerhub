﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.Entities
{
    public class Personnage
    {
        public int Id { get; set; }
        public int IdJoueur { get; set; }
        public string Nom { get; set; }
        public int Age { get; set; }
        public string Sexe { get; set; }
        public string Couleur { get; set; }
        public int FormeCheveux { get; set; }
        public string CouleurCheveux { get; set; }
        public int FormeBarbe { get; set; }
        public int CouleurBarbe { get; set; }
        public int FormeTop { get; set; }
        public int CouleurTop { get; set; }
        public int FormePant { get; set; }
        public int CouleurPant { get; set; }
        public int FormeShoes { get; set; }
        public int CouleurShoes { get; set; }
        public bool IsAlive { get; set; }
        public int Sante { get; set; }
        public int Sommeil { get; set; }
        public int Faim { get; set; }
        public int Hygiene { get; set; }
        public int Confort { get; set; }
        public int Divertissement { get; set; }
        public double Argent { get; set; }
    }
}
