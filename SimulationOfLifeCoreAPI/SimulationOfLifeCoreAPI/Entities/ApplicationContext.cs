﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.Entities
{
    public class ApplicationContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbSet<Joueurs> Joueurs { get; set; }
        public DbSet<Maitrises> Maitrises { get; set; }
        public DbSet<MaitrisesJoueur> MaitrisesJoueurs { get; set; }
        public DbSet<Succes> Succes { get; set; }
        public DbSet<SuccesJoueur> SuccesJoueurs { get; set; }
        public DbSet<Personnage> Personnages { get; set; }
        public DbSet<Statistiques> Statistiques { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Joueurs>()
                .HasIndex(p => new {p.Pseudo})
                .IsUnique(true);

            modelBuilder.Entity<Maitrises>();

            modelBuilder.Entity<MaitrisesJoueur>();

            modelBuilder.Entity<Succes>();

            modelBuilder.Entity<SuccesJoueur>();

            modelBuilder.Entity<Personnage>();

            modelBuilder.Entity<Statistiques>();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"workstation id=SimulationOfLife.mssql.somee.com;user id=Synyster7x_SQLLogin_1;pwd=82zyy7ja4r;data source=SimulationOfLife.mssql.somee.com;persist security info=False;initial catalog=SimulationOfLife");
            optionsBuilder.UseSqlServer(@"Data Source = MSI\SQLSERVER; Initial Catalog = SimulationOfLife; Persist Security Info = True; User ID = sa; Password = avenged7x");
        }
    }
}
