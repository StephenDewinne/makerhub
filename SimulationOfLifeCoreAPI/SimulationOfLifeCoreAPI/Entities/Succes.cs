﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.Entities
{
    public class Succes
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public int Recompense { get; set; }
        public bool IsSucceeded { get; set; }
    }
}
