﻿using SimulationOfLifeCoreAPI.DTO;
using SimulationOfLifeCoreAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.Services
{
    public class PersonnageServices
    {
        private readonly ApplicationContext _context;
        public PersonnageServices(ApplicationContext context)
        {
            _context = context;
        }

        public void Insert(PersonnageDTO dto)
        {
            Personnage personnage = new Personnage();
            personnage.IdJoueur = dto.IdJoueur;
            personnage.Nom = dto.Nom;
            personnage.Age = 18;
            personnage.Sexe = dto.Sexe;
            personnage.Couleur = dto.Couleur;
            personnage.FormeCheveux = dto.FormeCheveux;
            personnage.CouleurCheveux = dto.CouleurCheveux;
            personnage.FormeBarbe = dto.FormeBarbe;
            personnage.CouleurBarbe = dto.CouleurBarbe;
            personnage.FormeTop = dto.FormeTop;
            personnage.CouleurTop = dto.CouleurTop;
            personnage.FormePant = dto.FormePant;
            personnage.CouleurPant = dto.CouleurPant;
            personnage.FormeShoes = dto.FormeShoes;
            personnage.CouleurShoes = dto.CouleurShoes;
            personnage.IsAlive = dto.IsAlive;
            personnage.Sante = 100;
            personnage.Sommeil = 100;
            personnage.Faim = 100;
            personnage.Hygiene = 100;
            personnage.Confort = 100;
            personnage.Divertissement = 100;
            personnage.Argent = 2500.00;

            _context.Add(personnage);
            _context.SaveChanges();
        }
    }
}
