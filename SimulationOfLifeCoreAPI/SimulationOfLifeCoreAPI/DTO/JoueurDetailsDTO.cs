﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.DTO
{
    public class JoueurDetailsDTO
    {
        public int Id { get; set; }
        public string Pseudo { get; set; }
        public string MotDePasse { get; set; }
        public int PointsDeSurvie { get; set; }

        public IEnumerable<PersonnageDTO> Personnages { get; set; }

    }
}
