﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.DTO
{
    public class InfosAvatarDTO
    {
        public string Pseudo { get; set; }
        public string Avatar { get; set; }
    }
}
