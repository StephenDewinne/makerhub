﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.DTO
{
    public class CreationPersonnageDTO
    {
        public string Couleur { get; set; }
        public int CouleurBarbe { get; set; }
        public string CouleurCheveux { get; set; }
        public int CouleurPant { get; set; }
        public int CouleurShoes { get; set; }
        public int CouleurTop { get; set; }
        public int FormeBarbe { get; set; }
        public int FormeCheveux { get; set; }
        public int FormePant { get; set; }
        public int FormeShoes { get; set; }
        public int FormeTop { get; set; }
        public string Nom { get; set; }
        public string Sexe { get; set; }
        public string PseudoJoueur { get; set; }
    }
}
