﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.DTO
{
    public class JoueurDTO
    {
        public int Id { get; set; }
        public string Pseudo { get; set; }
        public string MotDePasse { get; set; }
        public int PointsDeSurvie { get; set; }
        public string Avatar { get; set; }

    }
}
