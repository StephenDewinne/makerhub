﻿using FluentValidation;
using SimulationOfLifeCoreAPI.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SimulationOfLifeCoreAPI.DTO
{
    public class PersonnageDTO
    {
        public int Id { get; set; }
        [Required]
        public int IdJoueur { get; set; }
        public string Nom { get; set; }
        public int Age { get; set; }
        public string Sexe { get; set; }
        public string Couleur { get; set; }
        public int FormeCheveux { get; set; }
        public string CouleurCheveux { get; set; }
        public int FormeBarbe { get; set; }
        public int CouleurBarbe { get; set; }
        public int FormeTop { get; set; }
        public int CouleurTop { get; set; }
        public int FormePant { get; set; }
        public int CouleurPant { get; set; }
        public int FormeShoes { get; set; }
        public int CouleurShoes { get; set; }
        public bool IsAlive { get; set; }
        public int Sante { get; set; }
        public int Sommeil { get; set; }
        public int Faim { get; set; }
        public int Hygiene { get; set; }
        public int Confort { get; set; }
        public int Divertissement { get; set; }
        public double Argent { get; set; }
    }

    public class PersonnageDTOValidator : AbstractValidator<PersonnageDTO>
    {
        public PersonnageDTOValidator(ApplicationContext dbContext)
        {
            RuleFor(dto => dto.Id).NotEmpty();
            RuleFor(m => m.IdJoueur).GreaterThanOrEqualTo(1).Custom((IdJoueur, context) =>
            {
                if (!dbContext.Joueurs.Any(e => e.Id == IdJoueur)) context.AddFailure("Joueur n'existe pas");
            });
        }
    }
}
