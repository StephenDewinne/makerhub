﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimulationOfLifeCoreAPI.Entities;

namespace SimulationOfLifeCoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuccesJoueurController : Controller
    {
        private readonly ApplicationContext _context;
        public SuccesJoueurController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<SuccesJoueur> Get()
        {
            return _context.SuccesJoueurs.ToList();
        }

        [HttpPost]
        public IActionResult Post(int idJoueur, int idSucces)
        {
            using (var db = new ApplicationContext())
            {
                var succesJoueur = new SuccesJoueur();
                succesJoueur.IdJoueur = idJoueur;
                succesJoueur.IdSucces = idSucces;

                db.Add(succesJoueur);
                db.SaveChanges();

                return Ok(_context.SuccesJoueurs.ToList());
            }
        }
    }
}