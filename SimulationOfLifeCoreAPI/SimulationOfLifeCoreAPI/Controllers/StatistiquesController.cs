﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimulationOfLifeCoreAPI.DTO;
using SimulationOfLifeCoreAPI.Entities;

namespace SimulationOfLifeCoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatistiquesController : Controller
    {
        private readonly ApplicationContext _context;
        public StatistiquesController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Statistiques> Get()
        {
            return _context.Statistiques.ToList();
        }

        [HttpPost]
        public IActionResult Post(StatistiquesDTO dto)
        {
            using (var db = new ApplicationContext())
            {
                var statistique = new Statistiques();
                statistique.IdPersonnage = dto.IdPersonnage;
                statistique.Conduite = 0;
                statistique.Cuisine = 0;
                statistique.JeuxVideos = 0;
                statistique.Programmation = 0;
                statistique.Ecriture = 0;
                statistique.Bricolage = 0;
                statistique.Jardinage = 0;
                statistique.Fitness = 0;
                statistique.Peche = 0;
                statistique.Logique = 0;
                statistique.Musique = 0;

                db.Add(statistique);
                db.SaveChanges();

                return Ok(_context.Statistiques.ToList());
            }
        }


        [HttpPut]
        public IActionResult Put(StatistiquesDTO dto, int idPersonnage)
        {
            using (var db = new ApplicationContext())
            {
                var statistique = _context.Statistiques.FirstOrDefault(x => x.IdPersonnage == idPersonnage);
                statistique.IdPersonnage = idPersonnage;
                statistique.Conduite = dto.Conduite;
                statistique.Cuisine = dto.Cuisine;
                statistique.JeuxVideos = dto.JeuxVideos;
                statistique.Programmation = dto.Programmation;
                statistique.Ecriture = dto.Ecriture;
                statistique.Bricolage = dto.Bricolage;
                statistique.Jardinage = dto.Jardinage;
                statistique.Fitness = dto.Fitness;
                statistique.Peche = dto.Peche;
                statistique.Logique = dto.Logique;
                statistique.Musique = dto.Musique;

                db.Update(statistique);
                db.SaveChanges();

                return Ok(_context.Personnages.ToList());
            }
        }
    }
}