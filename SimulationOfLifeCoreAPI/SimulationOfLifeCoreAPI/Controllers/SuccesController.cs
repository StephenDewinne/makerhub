﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimulationOfLifeCoreAPI.Entities;

namespace SimulationOfLifeCoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuccesController : Controller
    {
        private readonly ApplicationContext _context;
        public SuccesController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Succes> Get()
        {
            return _context.Succes.ToList();
        }

        [HttpPost]
        public IActionResult Post(string nom, string description, int recompense)
        {
            using (var db = new ApplicationContext())
            {
                var succes = new Succes();
                succes.Nom = nom;
                succes.Description = description;
                succes.Recompense = recompense;
                succes.IsSucceeded = false;

                db.Add(succes);
                db.SaveChanges();

                return Ok(_context.Succes.ToList());
            }
        }
    }
}