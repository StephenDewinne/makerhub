﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimulationOfLifeCoreAPI.Entities;

namespace SimulationOfLifeCoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaitrisesJoueurController : Controller
    {
        private readonly ApplicationContext _context;
        public MaitrisesJoueurController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Un truc");
        }

        [HttpPost]
        public IActionResult Post(int idJoueur, int idMaitrise)
        {
            using (var db = new ApplicationContext())
            {
                var maitrisesJoueur = new MaitrisesJoueur();
                maitrisesJoueur.IdJoueur = idJoueur;
                maitrisesJoueur.IdMaitrise = idMaitrise;

                db.Add(maitrisesJoueur);
                db.SaveChanges();

                return Ok(_context.MaitrisesJoueurs.ToList());
            }
        }
    }
}