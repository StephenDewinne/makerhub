﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimulationOfLifeCoreAPI.Entities;

namespace SimulationOfLifeCoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaitrisesController : Controller
    {
        private readonly ApplicationContext _context;
        public MaitrisesController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Maitrises> Get()
        {
            return _context.Maitrises.ToList();
        }

        [HttpPost]
        public IActionResult Post(string nom, string description, int prix)
        {
            using (var db = new ApplicationContext())
            {
                var maitrises = new Maitrises();
                maitrises.Nom = nom;
                maitrises.Description = description;
                maitrises.Prix = prix;
                maitrises.IsActive = false;
                
                db.Add(maitrises);
                db.SaveChanges();

                return Ok(_context.Maitrises.ToList());
            }
        }
    }
}