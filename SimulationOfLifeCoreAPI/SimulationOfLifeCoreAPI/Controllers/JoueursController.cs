﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimulationOfLifeCoreAPI.DTO;
using SimulationOfLifeCoreAPI.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SimulationOfLifeCoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JoueursController : ControllerBase
    {
        private readonly ApplicationContext _context;
        public JoueursController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_context.Joueurs.ToList());
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id)
        {
            Joueurs model = _context.Joueurs.FirstOrDefault(j => j.Id == id);
            IEnumerable<PersonnageDTO> perso = _context.Personnages.Where(j => j.IdJoueur == id)
                .Select(p => new PersonnageDTO
                {
                    Id = p.Id,
                    Age = p.Age
                });
            JoueurDetailsDTO dto = new JoueurDetailsDTO
            {
                Id = model.Id,
                Pseudo = model.Pseudo,
                PointsDeSurvie = model.PointsDeSurvie,
                Personnages = perso
            };
            return Ok(dto);
        }

        [HttpGet]
        [Route("{username}/{password}")]
        public string Get(string username, string password)
        {
            Joueurs model = _context.Joueurs.FirstOrDefault(j => j.Pseudo == username);

            try
            {
                using (var sha = new MD5CryptoServiceProvider())
                {
                    var data = Encoding.UTF8.GetBytes(password);
                    var shadata = sha.ComputeHash(data);
                    string hashedPassword = Encoding.UTF8.GetString(shadata);

                    if (model.MotDePasse == hashedPassword)
                    {
                        return "Valide";
                    }
                }
            }
            
            catch
            {
                return "BadPseudo";
            }

            return "BadPassword";
        }

        [HttpGet]
        [Route("Infos/{username}")]
        public IActionResult Get(string username)
        {
            IEnumerable<Infos> infos = _context.Joueurs.Where(j => j.Pseudo == username)
            .Select(p => new Infos
            {
                Pseudo = p.Pseudo,
                Avatar = p.Avatar,
                PointsDeSurvie = p.PointsDeSurvie
            });

            return Ok(infos);
        }


        [HttpPost]
        [Route("{username}/{password}")]
        public IActionResult Post(string username, string password)
        {

            using (var db = new ApplicationContext())
            {
                var reponse = new Reponse();
                
                var joueurs = new Joueurs();
                joueurs.Pseudo = username;
                joueurs.Avatar = "Avatar_Stephen";
                joueurs.PointsDeSurvie = 0;

                var sha = new MD5CryptoServiceProvider();
                var data = Encoding.UTF8.GetBytes(password);
                var shadata = sha.ComputeHash(data);
                string hashedPassword = Encoding.UTF8.GetString(shadata);
                joueurs.MotDePasse = hashedPassword;
                
                try
                {
                    db.Add(joueurs);
                    db.SaveChanges();
                }
                catch
                {
                    reponse.Message = "Le pseudo existe déjà";
                    return Ok(reponse);
                }
                reponse.Message = "Compte crée";
                return Ok(reponse);
            }
        }

        [HttpPut]
        public IActionResult PutAvatar(InfosAvatarDTO dto)
        {
            using (var db = new ApplicationContext())
            {
                var joueur = _context.Joueurs.FirstOrDefault(x => x.Pseudo == dto.Pseudo);
                joueur.Avatar = dto.Avatar;

                try
                {
                    db.Update(joueur);
                    db.SaveChanges();

                    return Ok(joueur);
                }

                catch
                {
                    return Ok("Impossible de sauvegarder les changements");
                }
            }
        }
    }
}