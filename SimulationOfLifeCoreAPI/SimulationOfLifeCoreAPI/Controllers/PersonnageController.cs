﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.IIS.Core;
using SimulationOfLifeCoreAPI.DTO;
using SimulationOfLifeCoreAPI.Entities;
using SimulationOfLifeCoreAPI.Services;

namespace SimulationOfLifeCoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonnageController : Controller
    {
        private readonly ApplicationContext _context;
        private readonly PersonnageServices _services;
        public PersonnageController(ApplicationContext context, PersonnageServices services)
        {
            _context = context;
            _services = services;
        }

        [HttpGet]
        public IEnumerable<Personnage> Get()
        {
            return _context.Personnages.ToList();
        }

        [HttpGet]
        [Route("UpdateKhun/{id}")]
        public Personnage UpdateKhun(int id)
        {
            Personnage model = _context.Personnages.FirstOrDefault(x => x.Id == id);
            var rand = new Random();

            if (model.Hygiene == 0)
            {
                model.Sante -= 2;
            }

            else if (model.Hygiene < 15)
            {
                model.Sante -= 1;
            }

            if (model.Faim == 0)
            {
                model.Sante -= 4;
            }

            else if (model.Faim < 30)
            {
                model.Sante -= 2;
            }

            model.Hygiene -= rand.Next(2, 6);
            model.Faim -= rand.Next(2, 5);

            if (model.Sante < 0)
            {
                model.Sante = 0;
            }

            if (model.Hygiene < 0)
            {
                model.Hygiene = 0;
            }

            if (model.Faim < 0)
            {
                model.Faim = 0;
            }

            

            using( var db = new ApplicationContext())
            {
                db.Update(model);
                db.SaveChanges();
            }

            return model;
        }

        [HttpPost]
        [Route("Khun")]
        public IEnumerable<Personnage> GetKhunAlive(GetKhunDTO dto)
        {
            var compte = _context.Joueurs.FirstOrDefault(x => x.Pseudo == dto.Pseudo);

            return _context.Personnages.Where(x => x.IsAlive == true && x.IdJoueur == compte.Id);
        }

        [HttpPost]
        public IActionResult Insert(CreationPersonnageDTO dto)
        {
            using (var db = new ApplicationContext())
            {
                var Personnage = new Personnage();

                Personnage.Age = 18;
                Personnage.Argent = 2500;
                Personnage.Confort = 100;
                Personnage.Couleur = dto.Couleur;
                Personnage.CouleurBarbe = dto.CouleurBarbe;
                Personnage.CouleurCheveux = dto.CouleurCheveux;
                Personnage.CouleurPant = dto.CouleurPant;
                Personnage.CouleurShoes = dto.CouleurShoes;
                Personnage.CouleurTop = dto.CouleurTop;
                Personnage.Divertissement = 100;
                Personnage.Faim = 100;
                Personnage.FormeBarbe = dto.FormeBarbe;
                Personnage.FormeCheveux = dto.FormeCheveux;
                Personnage.FormePant = dto.FormePant;
                Personnage.FormeShoes = dto.FormeShoes;
                Personnage.FormeTop = dto.FormeTop;
                Personnage.Hygiene = 100;
                Personnage.IsAlive = true;
                Personnage.Nom = dto.Nom;
                Personnage.Sante = 100;
                Personnage.Sexe = dto.Sexe;
                Personnage.Sommeil = 100;
                Personnage.IdJoueur = _context.Joueurs.FirstOrDefault(x => x.Pseudo == dto.PseudoJoueur).Id;
                

                try
                {
                    db.Add(Personnage);
                    db.SaveChanges();

                    return Ok();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
        }

        //[HttpPost]
        //public IActionResult Insert(PersonnageDTO dto,
        //    [FromServices] PersonnageDTOValidator validator)
        //{
        //    try
        //    {
        //        if (!ModelState.IsValid) return BadRequest("Model Invalide");

        //        var validationResult = validator.Validate(dto);
        //        if (!validationResult.IsValid) return BadRequest(validationResult.Errors);

        //        _services.Insert(dto);
        //        return Ok();
        //    }
        //    catch (Exception ex)
        //    {
        //        //Logger ex
        //        return this.StatusCode(500);
        //    }
        //}

        [HttpPut]
        public IActionResult Put(PersonnageDTO dto)
        {
            using (var db = new ApplicationContext())
            {
                var personnage = _context.Personnages.FirstOrDefault(x => x.Id == dto.Id);
                personnage.IdJoueur = dto.IdJoueur;
                personnage.Nom = dto.Nom;
                personnage.Age = dto.Age;
                personnage.Sexe = dto.Sexe;
                personnage.Couleur = dto.Couleur;
                personnage.FormeCheveux = dto.FormeCheveux;
                personnage.CouleurCheveux = dto.CouleurCheveux;
                personnage.FormeBarbe = dto.FormeBarbe;
                personnage.CouleurBarbe = dto.CouleurBarbe;
                personnage.FormeTop = dto.FormeTop;
                personnage.CouleurTop = dto.CouleurTop;
                personnage.FormePant = dto.FormePant;
                personnage.CouleurPant = dto.CouleurPant;
                personnage.FormeShoes = dto.FormeShoes;
                personnage.CouleurShoes = dto.CouleurShoes;
                personnage.IsAlive = dto.IsAlive;
                personnage.Sante = dto.Sante;
                personnage.Sommeil = dto.Sommeil;
                personnage.Faim = dto.Faim;
                personnage.Hygiene = dto.Hygiene;
                personnage.Confort = dto.Confort;
                personnage.Divertissement = dto.Divertissement;
                personnage.Argent = dto.Argent;

                db.Update(personnage);
                db.SaveChanges();

                return Ok(_context.Personnages.ToList());
            }
        }
    }
}